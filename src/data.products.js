import axios from 'axios'

export const productList = axios
    .get("http://api.alizehstore/products")
    .then(function (response) {
        // console.log(response);
        return response;
    })
    .catch(function (error) {
        console.log(error);
    });


export const productDetails = axios
    .get("http://api.alizehstore/products/1")
    .then(res => res.data);

export const productsData = async () => {
    try {
        return axios.get('http://api.alizehstore/products')
    } catch (error) {
        console.error(error)
    }
}