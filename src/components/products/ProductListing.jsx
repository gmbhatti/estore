import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Product from "./Product";

export class ProductListing extends Component {
  state = {
    products: []
  };
  componentDidMount() {
    //this.getAllProducts();
    axios.get("http://api.alizehstore/products").then(res => {
      this.setState({
        products: res.data
      });
    }); //*/
  }

  /*  getAllProducts = async () => {
    let res = await axios.get("http://api.alizehstore/products");
    let { data } = await res.data;
    this.setState({ products: data });
  };//*/

  render() {
    const { products } = this.state;
    const product_list = products.length ? (
      products.map(product => {
        return <Product key={product.id} product={product} />;
      })
    ) : (
      <p>Looding ....</p>
    );

    return (
      <div className="container">
        <div className="row">{product_list}</div>
      </div>
    );
  }
}

export default ProductListing;
