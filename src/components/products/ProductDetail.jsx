import React, { Component } from "react";
import { ProductConsumer } from "../../context";
import { Link } from "react-router-dom";
import { MDBBtn } from "mdbreact";
import axios from "axios";

export default class ProductDetail extends Component {
  state = {
    product: {}
  };
  componentDidMount() {
    axios.get("http://api.alizehstore/products/1").then(res => {
      console.log(res.data);
      this.setState({
        product: res.data
      });
    });
  }
  render() {
    return (
      <ProductConsumer>
        {value => {
          const {
            id,
            company,
            img,
            info,
            price,
            title,
            inCart
          } = value.detailProduct;
          const product = this.state.product;
          return (
            <div className="container py-5">
              <div className="row">
                <div className="col-10 mx-auto text-center my-5">
                  <h1>{product.title}</h1>
                  <div className="row">
                    <div className="col-10 mx-auto col-md-6 my-3">
                      <img
                        src={product.front_img}
                        className="img-fluid"
                        alt=""
                      />
                    </div>

                    <div className="col-10 mx-auto col-md-6 my-3 text-capitalize">
                      <h2>model: {product.title}</h2>
                      <h4 className="text-uppercase text-muted mt-3 mb-2">
                        made by:{" "}
                        <span className="text-uppercase">{product.brand}</span>
                      </h4>
                      <h4>
                        <strong>PKR</strong>
                        {product.price}
                      </h4>
                      <p className="text-captilize font-weight mt-3">
                        some info
                      </p>
                      <p className="text-muted lead">{product.breif}</p>
                      <div>
                        <Link to="/">
                          <MDBBtn>Back to Products</MDBBtn>
                        </Link>
                        <MDBBtn
                          disabled={inCart ? true : false}
                          onClick={() => {
                            value.addToCart(id);
                            value.openModel(id);
                          }}
                        >
                          {inCart ? "in cart" : "Add to cart"}t
                        </MDBBtn>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
        }}
      </ProductConsumer>
    );
  }
}
