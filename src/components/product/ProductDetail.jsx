import React, { Component } from 'react'
import {ProductConsumer} from '../../context';
import {Link } from 'react-router-dom';
import {MDBBtn} from 'mdbreact';


export default class ProductDetail extends Component {
  
  
  render() {
    return (
      <ProductConsumer>
        {value=>{
          const {id, company, img, info, price, title, inCart} =
          value.detailProduct;
          return (
             <div className="container py-5">
              <div className="row">
                <div className="col-10 mx-auto text-center my-5">
                  <h1>{title}</h1>
                  <div className="row">
                    <div className="col-10 mx-auto col-md-6 my-3">
                      <img src={img} className="img-fluid" alt="" />
                    </div>

                    <div className="col-10 mx-auto col-md-6 my-3 text-capitalize">
                      <h2>model: {title}</h2>
                      <h4 className="text-uppercase text-muted mt-3 mb-2">
                        made by: <span className="text-uppercase">
                          {company}
                        </span>
                      </h4>
                      <h4>
                        <strong>$</strong>
                        {price}
                      </h4>
                      <p className="text-captilize font-weight mt-3">
                        some info
                      </p>
                      <p className="text-muted lead">{info}</p>
                      <div>
                        <Link to="/"><MDBBtn>Back to Products</MDBBtn></Link>
                        <MDBBtn disabled={inCart ? true:false} onClick={()=>{
                          value.addToCart(id);
                          value.openModel(id);
                        }}>{inCart ? "in cart" : "Add to cart"}t</MDBBtn>
                      
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
        }}
      </ProductConsumer>
    )
  }
}
