import React from 'react'

export default function BannerUTube() {
  return (
    <div className="row">
      <video loop autoPlay style={{ width: `100%` }} >
        <source src="https://os.alipayobjects.com/rmsportal/CoDFvoxaQpTnLOM.mp4" type="video/mp4"/>
          </video>
    </div>
  )
}
