import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  MDBNavbar,
  MDBBtn,
  MDBNavbarBrand,
  MDBIcon,
  MDBBadge,
  MDBNavbarNav,
  MDBNavItem,
  MDBNavLink,
  MDBNavbarToggler,
  MDBCollapse,
  MDBFormInline,
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem
} from "mdbreact";
import logo from "../../logo.png";
import { ProductConsumer } from "../../context";

export default class TopNavbar extends Component {
  state = {
    isOpen: false
  };

  toggleCollapse = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  render() {
    return (
      <MDBNavbar color="default-color" dark expand="md">
        <MDBNavbarBrand>
          <strong className="white-text">
            <Link to="/" className="white-text">
              <img
                src={logo}
                alt=""
                className="navbar-brand"
                style={{ maxHeight: 40 + "px" }}
              />
              Alizeh Store
            </Link>
          </strong>
        </MDBNavbarBrand>
        <MDBNavbarToggler onClick={this.toggleCollapse} />
        <MDBCollapse id="navbarCollapse3" isOpen={this.state.isOpen} navbar>
          <MDBNavbarNav left>
            <MDBNavItem active>
              <MDBNavLink to="/">Home</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink to="/about">About</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink to="/cart">Cart</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBDropdown>
                <MDBDropdownToggle nav caret>
                  <div className="d-none d-md-inline">Components</div>
                </MDBDropdownToggle>
                <MDBDropdownMenu right>
                  <MDBDropdownItem href="/slider">Carrousal</MDBDropdownItem>
                  <MDBDropdownItem href="/psslider">
                    Photo Swipe
                  </MDBDropdownItem>
                  <MDBDropdownItem href="/lightbox">Lightbox</MDBDropdownItem>
                  <MDBDropdownItem href="#!">
                    Something else here
                  </MDBDropdownItem>
                </MDBDropdownMenu>
              </MDBDropdown>
            </MDBNavItem>
          </MDBNavbarNav>
          <MDBNavbarNav right>
            <MDBNavItem>
              <MDBFormInline waves>
                <div className="md-form my-0">
                  <input
                    className="form-control mr-sm-2"
                    type="text"
                    placeholder="Search"
                    aria-label="Search"
                  />
                </div>
              </MDBFormInline>
            </MDBNavItem>

            <MDBNavItem>
              <MDBNavLink to="/cart">
                <ProductConsumer>
                  {value => {
                    const { cart } = value;
                    if (cart.length > 0) {
                      const items = cart.length;
                      const loaded = true;
                      return (
                        <MDBBtn rounded floating outline color="danger">
                          <MDBIcon size="2x" icon="cart-plus">
                            <MDBBadge
                              size="1x"
                              color={loaded ? "danger" : "primary"}
                              className="ml-2"
                            >
                              {items}
                            </MDBBadge>{" "}
                            My Cart{" "}
                          </MDBIcon>
                        </MDBBtn>
                      );
                    }
                  }}
                </ProductConsumer>
              </MDBNavLink>
            </MDBNavItem>
          </MDBNavbarNav>
        </MDBCollapse>
      </MDBNavbar>
    );
  }
}
