import React, { Component } from "react";
import Lightbox from "react-lightbox-component";

export default class LBSlider extends Component {
  state = {
    items: [
      {
        src: "http://lorempixel.com/1200/900/sports/1",
        w: 1200,
        h: 900,
        title: "Image 1"
      },
      {
        src: "http://lorempixel.com/1200/900/sports/2",
        w: 1200,
        h: 900,
        title: "Image 2"
      },
      {
        src: "http://lorempixel.com/1200/900/sports/3",
        w: 1200,
        h: 900,
        title: "Image 3"
      }
    ],
    galleryItems: [
      {
        src: "http://lorempixel.com/1200/900/nightlife/1",
        thumbnail: "http://lorempixel.com/120/90/nightlife/1",
        w: 1200,
        h: 900,
        title: "Image 1"
      },
      {
        src: "http://lorempixel.com/1200/900/nightlife/2",
        thumbnail: "http://lorempixel.com/120/90/nightlife/2",
        w: 1200,
        h: 900,
        title: "Image 2"
      },
      {
        src: "http://lorempixel.com/1200/900/nightlife/3",
        thumbnail: "http://lorempixel.com/120/90/nightlife/3",
        w: 1200,
        h: 900,
        title: "Image 3"
      },
      {
        src: "http://lorempixel.com/1200/900/nightlife/4",
        thumbnail: "http://lorempixel.com/120/90/nightlife/4",
        w: 1200,
        h: 900,
        title: "Image 4"
      }
    ]
  };

  render() {
    return (
      <div>
        <Lightbox
          images={this.state.items}
          
        />
      </div>
    );
  }
}
