import React from "react";

export default function CartItem({ item, value }) {
  const { id, title, img, price, total, count } = item;
  const { increment, decrement, removeItem } = value;
  return (
    <div className="row my-1 text-capitalize text-center">
      <div className="col-10 mx-auto col-lg-2">
        <img
          src={img}
          className="img-fluid"
          style={{ width: "5rem", height: "5rem" }}
          alt=""
        />
      </div>
      <div className="col-10 mx-auto col-lg-2">
        <span className="d-lg-none">product: </span> {title}
      </div>
      <div className="col-10 mx-auto col-lg-2">{price}</div>
      <div className="co l-10 mx-auto col-lg-2 my-2 my-lg-0">
        <div className="d-flex justify-content-center">
          <div>
            <span
              className=" btn-black mx-auto"
              onClick={() => decrement(id)}
            >
              -
            </span>
            <span className=" btn-black mx-1">{count}</span>
            <span
              className=" btn-black mx-auto"
              onClick={() => increment(id)}
            >
              +
            </span>
          </div>
        </div>
      </div>
      <div className="col-10 mx-auto col-lg-2">
        <div className="cart-icon" onClick={() => removeItem(id)}>
          <i className="fa fa-trash" />
        </div>
      </div>
      <div className="col-10 mx-auto col-lg-2">
        <strong>{total}</strong>
      </div>
    </div>
  );
}
