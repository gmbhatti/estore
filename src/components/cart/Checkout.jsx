import React, { Component } from "react";

export class Checkout extends Component {
  state = {
    activeItem: "1"
  };

  toggle = tab => () => {
    if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
    }
  };
  handleSubmit = e => {
    e.preventDefault();
    console.log(e.target);
  };

  render() {
    return (
      <div className="container">
        <h3>Checkout</h3>
        <div className="card">
          <div className="card-body">
            {/*<!--Grid row-->*/}
            <div className="row">
              {/*<!--Grid column-->*/}
              <div className="col-lg-8 mb-4">
                {/*<!-- Pills navs -->*/}
                <ul className="nav md-pills nav-justified pills-primary font-weight-bold">
                  <li className="nav-item">
                    <a
                      className={
                        this.state.activeItem === "1"
                          ? "nav-link active show"
                          : "`nav-link"
                      }
                      onClick={this.toggle("1")}
                      data-toggle="tab"
                      href="#tabCheckoutBilling123"
                      role="tab"
                    >
                      1. Billing
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className={
                        this.state.activeItem === "2"
                          ? "nav-link active show"
                          : "`nav-link"
                      }
                      onClick={this.toggle("2")}
                      data-toggle="tab"
                      href="#tabCheckoutAddons123"
                      role="tab"
                    >
                      2. Addons
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className={
                        this.state.activeItem === "3"
                          ? "nav-link active show"
                          : "`nav-link"
                      }
                      onClick={this.toggle("3")}
                      data-toggle="tab"
                      href="#tabCheckoutPayment123"
                      role="tab"
                    >
                      3. Payment
                    </a>
                  </li>
                </ul>

                {/*<!-- Pills panels -->*/}
                <div className="tab-content pt-4">
                  {/*<!--Panel 1-->*/}
                  <div
                    className={
                      this.state.activeItem === "1"
                        ? "tab-pane fade in show active"
                        : "tab-pane fade"
                    }
                    id="1"
                    role="tabpanel"
                  >
                    {/*<!--Card content-->*/}
                    <form>
                      {/*<!--Grid row-->*/}
                      <div className="row">
                        {/*<!--Grid column-->*/}
                        <div className="col-md-6 mb-4">
                          {/*<!--firstName-->*/}
                          <label htmlFor="firstName" className="">
                            First name
                          </label>
                          <input
                            type="text"
                            id="firstName"
                            className="form-control"
                          />
                        </div>
                        {/*<!--Grid column-->*/}

                        {/*<!--Grid column-->*/}
                        <div className="col-md-6 mb-2">
                          {/*<!--lastName-->*/}
                          <label htmlFor="lastName" className="">
                            Last name
                          </label>
                          <input
                            type="text"
                            id="lastName"
                            className="form-control"
                          />
                        </div>
                        {/*<!--Grid column-->*/}
                      </div>
                      {/*<!--Grid row-->*/}

                      {/*<!--Username-->*/}
                      <div className="input-group mb-4">
                        <div className="input-group-prepend">
                          <span className="input-group-text" id="basic-addon1">
                            @
                          </span>
                        </div>
                        <input
                          type="text"
                          className="form-control py-0"
                          placeholder="Username"
                          aria-describedby="basic-addon1"
                        />
                      </div>

                      {/*<!--email-->*/}
                      <label htmlFor="email" className="">
                        Email (optional)
                      </label>
                      <input
                        type="text"
                        id="email"
                        className="form-control mb-4"
                        placeholder="youremail@example.com"
                      />

                      {/*<!--address-->*/}
                      <label htmlFor="address" className="">
                        Address
                      </label>
                      <input
                        type="text"
                        id="address"
                        className="form-control mb-4"
                        placeholder="1234 Main St"
                      />

                      {/*<!--address-2-->*/}
                      <label htmlFor="address-2" className="">
                        Address 2 (optional)
                      </label>
                      <input
                        type="text"
                        id="address-2"
                        className="form-control mb-4"
                        placeholder="Apartment or suite"
                      />

                      {/*<!--Grid row-->*/}
                      <div className="row">
                        {/*<!--Grid column-->*/}
                        <div className="col-lg-4 col-md-12 mb-4">
                          <label htmlFor="country">Country</label>
                          <select
                            className="custom-select d-block w-100"
                            id="country"
                            required
                          >
                            <option value="">Choose...</option>
                            <option>United States</option>
                          </select>
                          <div className="invalid-feedback">
                            Please select a valid country.
                          </div>
                        </div>
                        {/*<!--Grid column-->*/}

                        {/*<!--Grid column-->*/}
                        <div className="col-lg-4 col-md-6 mb-4">
                          <label htmlFor="state">State</label>
                          <select
                            className="custom-select d-block w-100"
                            id="state"
                            required
                          >
                            <option value="">Choose...</option>
                            <option>California</option>
                          </select>
                          <div className="invalid-feedback">
                            Please provide a valid state.
                          </div>
                        </div>
                        {/*<!--Grid column-->*/}

                        {/*<!--Grid column-->*/}
                        <div className="col-lg-4 col-md-6 mb-4">
                          <label htmlFor="zip">Zip</label>
                          <input
                            type="text"
                            className="form-control"
                            id="zip"
                            placeholder=""
                            required
                          />
                          <div className="invalid-feedback">
                            Zip code required.
                          </div>
                        </div>
                        {/*<!--Grid column-->*/}
                      </div>
                      {/*<!--Grid row-->*/}

                      <hr />

                      <div className="mb-1">
                        <input
                          type="checkbox"
                          className="form-check-input filled-in"
                          id="chekboxRules"
                        />
                        <label
                          className="form-check-label"
                          htmlFor="chekboxRules"
                        >
                          I accept the terms and conditions
                        </label>
                      </div>
                      <div className="mb-1">
                        <input
                          type="checkbox"
                          className="form-check-input filled-in"
                          id="safeTheInfo"
                        />
                        <label
                          className="form-check-label"
                          htmlFor="safeTheInfo"
                        >
                          Save this information for next time
                        </label>
                      </div>
                      <div className="mb-1">
                        <input
                          type="checkbox"
                          className="form-check-input filled-in"
                          id="subscribeNewsletter"
                        />
                        <label
                          className="form-check-label"
                          htmlFor="subscribeNewsletter"
                        >
                          Subscribe to the newsletter
                        </label>
                      </div>

                      <hr />

                      <button
                        className="btn btn-primary btn-lg btn-block"
                        type="submit"
                        id="submit1"
                        onClick={this.hadleSubmit}
                      >
                        Next step
                      </button>
                    </form>
                  </div>
                  {/*<!--/.Panel 1-->*/}

                  {/*<!--Panel 2-->*/}
                  <div
                    className={
                      this.state.activeItem === "2"
                        ? "tab-pane fade in show active"
                        : "tab-pane fade"
                    }
                    id="tabCheckoutAddons123"
                    role="tabpanel"
                  >
                    {/*<!--Grid row-->*/}
                    <div className="row">
                      {/*<!--Grid column-->*/}
                      <div className="col-md-5 mb-4">
                        <img
                          src="https://mdbootstrap.com/img/Photos/Others/images/43.jpg"
                          className="img-fluid z-depth-1-half"
                          alt="Second sample "
                        />
                      </div>
                      {/*<!--Grid column-->*/}

                      {/*<!--Grid column-->*/}
                      <div className="col-md-7 mb-4">
                        <h5 className="mb-3 h5">Additional premium support</h5>

                        <p>
                          Lorem ipsum dolor sit amet consectetur adipisicing
                          elit. Veritatis, ea ut aperiam corrupti, dolorem.
                        </p>

                        {/*<!--Name-->*/}
                        <select className="mdb-select colorful-select dropdown-info">
                          <option value="" disabled>
                            Choose a period of time
                          </option>
                          <option value="1" defaultValue>
                            +6 months : 200$
                          </option>
                          <option value="2">+12 months: 400$</option>
                          <option value="3">+18 months: 800$</option>
                          <option value="4">+24 months: 1200$</option>
                        </select>

                        <button
                          type="button"
                          className="btn btn-primary btn-md"
                        >
                          Add premium support to the cart
                        </button>
                      </div>
                      {/*<!--Grid column-->*/}
                    </div>
                    {/*<!--Grid row-->*/}

                    <hr className="mb-5" />

                    {/*<!--Grid row-->*/}
                    <div className="row">
                      {/*<!--Grid column-->*/}
                      <div className="col-md-5 mb-4">
                        <img
                          src="https://mdbootstrap.com/img/Photos/Others/images/44.jpg"
                          className="img-fluid z-depth-1-half"
                          alt="Second sample"
                        />
                      </div>
                      {/*<!--Grid column-->*/}

                      {/*<!--Grid column-->*/}
                      <div className="col-md-7 mb-4">
                        <h5 className="mb-3 h5">MDB Membership</h5>

                        <p>
                          Lorem ipsum dolor sit amet consectetur adipisicing
                          elit. Veritatis, ea ut aperiam corrupti, dolorem.
                        </p>

                        {/*<!--Name-->*/}
                        <select className="mdb-select colorful-select dropdown-info">
                          <option value="" disabled>
                            Choose a period of time
                          </option>
                          <option value="1" defaultValue>
                            +6 months : 200$
                          </option>
                          <option value="2">+12 months: 400$</option>
                          <option value="3">+18 months: 800$</option>
                          <option value="4">+24 months: 1200$</option>
                        </select>

                        <button
                          type="button"
                          className="btn btn-primary btn-md"
                        >
                          Add MDB Membership to the cart
                        </button>
                      </div>
                      {/*<!--Grid column-->*/}
                    </div>
                    {/*<!--Grid row-->*/}

                    <hr className="mb-4" />

                    <button
                      className="btn btn-primary btn-lg btn-block"
                      type="submit"
                      id="submit2"
                      onClick={this.hadleSubmit}
                    >
                      Next step
                    </button>
                  </div>
                  {/*<!--/.Panel 2-->*/}

                  {/*<!--Panel 3-->*/}
                  <div
                    className={
                      this.state.activeItem === "3"
                        ? "tab-pane fade in show active"
                        : "tab-pane fade"
                    }
                    id="tabCheckoutPayment123"
                    role="tabpanel"
                  >
                    <div className="d-block my-3">
                      <div className="mb-2">
                        <input
                          name="group2"
                          type="radio"
                          className="form-check-input with-gap"
                          id="radioWithGap4"
                          defaultChecked
                          required
                        />
                        <label
                          className="form-check-label"
                          htmlFor="radioWithGap4"
                        >
                          Credit card
                        </label>
                      </div>
                      <div className="mb-2">
                        <input
                          iname="group2"
                          type="radio"
                          className="form-check-input with-gap"
                          id="radioWithGap5"
                          required
                        />
                        <label
                          className="form-check-label"
                          htmlFor="radioWithGap5"
                        >
                          Debit card
                        </label>
                      </div>
                      <div className="mb-2">
                        <input
                          name="group2"
                          type="radio"
                          className="form-check-input with-gap"
                          id="radioWithGap6"
                          required
                        />
                        <label
                          className="form-check-label"
                          htmlFor="radioWithGap6"
                        >
                          Paypal
                        </label>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6 mb-3">
                        <label htmlFor="cc-name123">Name on card</label>
                        <input
                          type="text"
                          className="form-control"
                          id="cc-name123"
                          placeholder=""
                          required
                        />
                        <small className="text-muted">
                          Full name as displayed on card
                        </small>
                        <div className="invalid-feedback">
                          Name on card is required
                        </div>
                      </div>
                      <div className="col-md-6 mb-3">
                        <label htmlFor="cc-number123">Credit card number</label>
                        <input
                          type="text"
                          className="form-control"
                          id="cc-number123"
                          placeholder=""
                          required
                        />
                        <div className="invalid-feedback">
                          Credit card number is required
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-3 mb-3">
                        <label htmlFor="cc-expiration123">Expiration</label>
                        <input
                          type="text"
                          className="form-control"
                          id="cc-expiration123"
                          placeholder=""
                          required
                        />
                        <div className="invalid-feedback">
                          Expiration date required
                        </div>
                      </div>
                      <div className="col-md-3 mb-3">
                        <label htmlFor="cc-cvv123">CVV</label>
                        <input
                          type="text"
                          className="form-control"
                          id="cc-cvv123"
                          placeholder=""
                          required
                        />
                        <div className="invalid-feedback">
                          Security code required
                        </div>
                      </div>
                    </div>
                    <hr className="mb-4" />

                    <button
                      className="btn btn-primary btn-lg btn-block"
                      type="submit"
                      id="submit3"
                      onClick={this.hadleSubmit}
                    >
                      Place order
                    </button>
                  </div>
                  {/*<!--/.Panel 3-->*/}
                </div>
                {/*<!-- Pills panels -->*/}
              </div>
              {/*<!--Grid column-->*/}

              {/*<!--Grid column-->*/}
              <div className="col-lg-4 mb-4">
                <button
                  className="btn btn-primary btn-lg btn-block"
                  type="submit"
                >
                  Place order
                </button>

                {/*<!--Card-->*/}
                <div className="card">
                  {/*<!--Card content-->*/}
                  <div className="card-body">
                    <h4 className="mb-4 mt-1 h5 text-center font-weight-bold">
                      Summary
                    </h4>

                    <hr />

                    <dl className="row">
                      <dd className="col-sm-8">
                        MDBootstrap UI KIT (jQuery version) - License 6-10
                        poeple + unlimited projects
                      </dd>
                      <dd className="col-sm-4">$ 2000</dd>
                    </dl>

                    <hr />

                    <dl className="row">
                      <dd className="col-sm-8">Premium support - 2 years</dd>
                      <dd className="col-sm-4">$ 2000</dd>
                    </dl>

                    <hr />

                    <dl className="row">
                      <dd className="col-sm-8">MDB Membership - 2 years</dd>
                      <dd className="col-sm-4">$ 2000</dd>
                    </dl>

                    <hr />

                    <dl className="row">
                      <dt className="col-sm-8">Total</dt>
                      <dt className="col-sm-4">$ 2000</dt>
                    </dl>
                  </div>
                </div>
                {/*<!--/.Card-->*/}
              </div>
              {/*<!--Grid column-->*/}
            </div>
            {/*<!--Grid row-->*/}
          </div>
        </div>
      </div>
    );
  }
}

export default Checkout;
