import React from "react";

const TermsPage = () => {
  return (
    <div>
      <main className="main-content" style={{ textAlign: "left" }}>
        <header className="page-header">
          <div className="breadcrumbs-container">
            <ul className="breadcrumbs">
              <li className="breadcrumb is-active">
                <a
                  href="https://shoprevelry.com/"
                  className="breadcrumb-label breadcrumb-link"
                >
                  <span itemprop="name">Home</span>
                </a>
              </li>
            </ul>
          </div>
          <h1 className="page-title">TERMS</h1>
        </header>

        <section className="section">
          <div className="container container-small">
            <article className="page-content cms-page">
              <p align="center">
                <strong>
                  <span style={{ textDecoration: "underline" }}>
                    <span>TERMS OF USE</span>
                  </span>
                </strong>
              </p>
              <p className="FreeForm">
                <span>
                  This Terms of Use Agreement (this “Agreement”) is a legal
                  agreement between you (“you” or “your”) and Revelry Corp.
                  (“Revelry”), its affiliates, and each of their respective
                  successors and assigns governing your use of ShopRevelry.com
                  (the “Site”). You hereby agree implicitly and without express
                  written consent that you have read and agree to be bound by
                  the outlined terms and conditions. If you do not agree to
                  abide by these Terms of Use, please refrain from using
                  ShopRevelry.com.
                </span>
              </p>
              <p className="FreeForm">
                <span>
                  Revelry reserves the right to modify the terms and conditions
                  of this Agreement or its policies relating to the Site at any
                  time, effective upon posting of an updated version of this
                  Agreement on the Site. You are responsible for regularly
                  reviewing this Agreement. Continued use of the Site after any
                  such changes shall constitute your consent to such changes.
                </span>
              </p>
              <p>
                <strong>
                  <span style={{ textDecoration: "underline" }}>
                    <span>Prices &amp; Orders</span>
                  </span>
                </strong>
              </p>
              <p>
                <span>
                  All prices displayed on the Revelry website are quoted in U.S.
                  Dollars. Revelry may restrict delivery to addresses within the
                  United States and Canada. Revelry will add shipping and
                  handling fees and applicable sales/use tax as necessary. All
                  orders shipped within Texas will be charged with the state
                  sales tax. The current Texas sales tax rate is 8.25%.&nbsp;
                  Customers ordering items to be shipped outside of Texas are
                  responsible for all sales taxes and duties as assessed by
                  their respective legislative authorities. Revelry reserves the
                  right without prior notice to discontinue or change
                  specifications and prices on products and services offered on
                  the Revelry website without incurring any obligation to you.
                  Products displayed on the Revelry website are available while
                  supplies last. The receipt by you of an order confirmation
                  does not constitute Revelry’s acceptance of an order. Prior to
                  Revelry’s acceptance of an order, verification of information
                  may be required. Revelry reserves the right at any time after
                  receipt of your order to accept or decline your order, or any
                  portion thereof, even after your receipt of an order
                  confirmation from Revelry, for any reason. Revelry reserves
                  the right to limit the order quantity on any item and to
                  refuse service to any customer without prior notification. In
                  the event that a product or service is listed at an incorrect
                  price due to supplier pricing information or typographical
                  error, Revelry shall have the right to refuse or cancel orders
                  placed for the product listed at the incorrect price,
                  regardless of whether the order has been confirmed and your
                  credit card charged. If your credit card has already been
                  charged for the purchase and your order is canceled, Revelry
                  shall promptly issue a credit to your credit card account in
                  the amount of the incorrect price.
                </span>
              </p>
              <p>
                <strong>
                  <span style={{ textDecoration: "underline" }}>
                    Shipping Risks
                  </span>
                </strong>
              </p>
              <p>
                Revelry will provide customers with tracking information
                including expected shipping dates, but Revelry will not be held
                responsible for any delays, damages or losses due to (but not
                limited to) natural disasters, acts of federal, state or local
                government, fires, floods, strikes, lockouts, freight embargoes,
                and acts of God. Customers can pursue claims for lost and
                damaged packages through the shipping courier.
              </p>
              <p>
                <strong>
                  <span style={{ textDecoration: "underline" }}>
                    Return and Refund Policy
                  </span>
                </strong>
              </p>
              <p>
                <span>
                  Revelry provides custom clothing orders and reserves the right
                  to refuse any returns on product that has been produced
                  explicitly for the customer.&nbsp; Each return is considered
                  on a case by case basis.&nbsp;{" "}
                </span>
              </p>
              <p>
                <strong>
                  <span style={{ textDecoration: "underline" }}>
                    Termination of Use
                  </span>
                </strong>
              </p>
              <p>
                <span>
                  Revelry may, in its sole discretion, terminate your account or
                  your use of the Site at anytime. You are personally liable for
                  any orders that you place or charges that you incur prior to
                  termination. Revelry reserves the right to change, suspend or
                  discontinue all or any aspects of the Site at any time without
                  prior notice.
                </span>
              </p>
              <p>
                <strong>
                  <span style={{ textDecoration: "underline" }}>
                    Collections
                  </span>
                </strong>
              </p>
              <p>
                In the event Revelry must pursue collections of any debt owed to
                the company resulting from a sales transaction, not limited to
                fraud, credit card reversals, credit accounts, etc., Revelry
                will be entitled to actual costs associated with the
                collections, including, but not limited to, actual legal fees
                and costs, travel fees, airfare, mileage, lodging, and other
                necessary costs associated with the collections.
              </p>
              <p className="FreeForm">
                <strong>
                  <span style={{ textDecoration: "underline" }}>Postings</span>
                </strong>
              </p>
              <p className="FreeForm">
                <span>
                  Revelry may from time to time monitor, review, and in its
                  discretion edit or delete, discussions, chats, profiles, and
                  postings on our Site; however, Revelry is under no obligation
                  to do so and assumes no responsibility or liability arising
                  from the content of any such transmissions or for any error,
                  defamation, libel, slander, omission, falsehood, obscenity,
                  pornography, profanity, hate speech, danger, illegality,
                  solicitations or inaccuracy contained in any information
                  transmitted to any such locations on our Site.&nbsp; Revelry
                  will cooperate with law enforcement or a court order
                  requesting or directing Revelry to disclose the identity of
                  anyone posting any information or material prohibited by this
                  Agreement.&nbsp; Revelry may also disclose such information if
                  such disclosure is reasonably necessary to protect the rights,
                  property, or personal safety of Revelry, its clients, or the
                  public.
                </span>
              </p>
              <p className="FreeForm">
                <strong>
                  <span style={{ textDecoration: "underline" }}>
                    <span>User Comments, Feedback and Other Submissions</span>
                  </span>
                </strong>
              </p>
              <p className="FreeForm">
                <span>
                  All comments, feedback, suggestions, ideas, and other
                  submissions disclosed, submitted, or offered to Revelry on or
                  by this Site or otherwise disclosed, submitted, or offered in
                  connection with your use of this Site or otherwise
                  (collectively, "Comments") shall be and remain Revelry’s
                  property.&nbsp; Such disclosure, submission, or offer of any
                  Comments shall constitute an assignment to Revelry of all
                  worldwide rights, titles, and interests in all copyrights and
                  other intellectual property in the Comments.&nbsp; Thus,
                  Revelry will own exclusively all such rights, title, and
                  interest and shall not be limited in any way in its use,
                  commercial or otherwise, of any Comments.&nbsp; Revelry is and
                  shall be under no obligation (1) to maintain any Comments in
                  confidence; (2) to pay to user any compensation for any
                  Comments; or (3) to respond to any Comments.
                </span>
              </p>
              <p>
                <strong>
                  <span style={{ textDecoration: "underline" }}>
                    <span>Compliance with Laws</span>
                  </span>
                </strong>
              </p>
              <p>
                <span>
                  You agree to comply with all applicable laws, statutes,
                  ordinances and regulations regarding your use of the Revelry
                  website and your purchase of items from Revelry (if
                  applicable) on the Revelry website.
                </span>
              </p>
              <p>
                <strong>
                  <span style={{ textDecoration: "underline" }}>
                    Intellectual Property Ownership
                  </span>
                </strong>
              </p>
              <p>
                <span>
                  All right, title and interest in the Site, including all
                  copyrights, patents, trade secrets, trade dress and other
                  proprietary rights, and any derivative works thereof, shall
                  belong solely and exclusively to Revelry or its licensors, and
                  you shall have no rights whatsoever in any of the foregoing.
                  All design and content featured on the Revelry website,
                  including the text, images, photographs, graphics, logos,
                  button icons, audio clips, information, forms, graphs, videos,
                  typefaces, music, sounds, illustrations, descriptions, data,
                  and other material and software, as well as the selection,
                  assembly and arrangement thereof, are referred to collectively
                  as the "content", and are copyrights, trademarks, trade dress,
                  and/or intellectual property that are owned, controlled, or
                  licensed by Revelry. This website in its entirety is protected
                  by copyright and applicable trade dress. The content may
                  contain errors, omissions, or typographical errors or may be
                  out of date. Revelry may change, delete, or update any content
                  at any time and without prior notice. The content is provided
                  for informational purposes only and is not binding on Revelry
                  in any way except to the extent it is specifically indicated
                  to be so. You may view and use the content only for your
                  personal, noncommercial use, and for shopping and ordering on
                  the Revelry website, and for no other purpose, and you shall
                  retain intact all copyright and other proprietary notices.
                  Except as provided in the foregoing, Revelry does not grant to
                  you or any person any right to use, reproduce, copy, modify,
                  transmit, display, publish, sell, license, create derivative
                  works, publicly perform, create derivative works from, or
                  distribute by any means, method, or process whatsoever, now
                  known or hereafter developed, any of the content on or
                  transmitted through the Revelry website, including without
                  limitation by transferring, downloading or otherwise copying
                  any content onto any disk drive or other storage medium. Any
                  use of the content, except as specifically permitted in these
                  terms, or as otherwise expressly permitted in the content or
                  in writing signed by Revelry, is strictly prohibited. You
                  understand and acknowledge that unauthorized disclosure, use
                  or copying of the proprietary products and services provided
                  pursuant to this Agreement may cause Revelry and its licensors
                  irreparable injury, which may not be remedied at law, and you
                  agree that Revelry and its licensors' remedies for breach of
                  this Agreement may be in equity by way of injunctive or other
                  equitable relief.
                </span>
              </p>
              <p>
                <strong>
                  <span style={{ textDecoration: "underline" }}>
                    <span>Linked Third Party Sites</span>
                  </span>
                </strong>
              </p>
              <p>
                <span>
                  Links to other internet websites operated by third parties do
                  not constitute sponsorship, endorsement, or approval by
                  Revelry of the content, policies, or practices of such linked
                  sites. Linked sites are not operated, controlled, or
                  maintained by Revelry, and Revelry is not responsible for the
                  availability, content, security, policies, or practices of
                  linked sites, including without limitation privacy policies
                  and practices. Links to other sites are provided for your
                  convenience only, and you access them at your own risk.{" "}
                </span>
                <span>
                  We encourage you to carefully read the privacy statement of
                  any Web site you visit.
                </span>
              </p>
              <p>
                <strong>
                  <span style={{ textDecoration: "underline" }}>
                    <span>Disclaimer of Warranties</span>
                  </span>
                </strong>
              </p>
              <p>
                <span>
                  EXCEPT AS OTHERWISE EXPRESSLY PROVIDED IN THIS AGREEMENT, (A)
                  THE SITE IS PROVIDED “AS-IS” AND “WITH ALL FAULTS,” AND, TO
                  THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, REVELRY,
                  INCLUDING ITS AFFILIATES, SUBSIDIARIES, LICENSORS,
                  SUBCONTRACTORS, DISTRIBUTORS, SERVICES PARTNERS, AGENTS AND
                  MARKETING PARTNERS) AND EACH OF THEIR RESPECTIVE EMPLOYEES,
                  DIRECTORS AND OFFICERS (COLLECTIVELY, THE “REVELRY PARTIES”)
                  DISCLAIM ALL REPRESENTATIONS, WARRANTIES AND CONDITIONS OF ANY
                  KIND, EXPRESS OR IMPLIED, REGARDING THE SITE, OR OTHERWISE
                  RELATING TO THIS AGREEMENT, INCLUDING WARRANTIES AND
                  CONDITIONS OF FITNESS FOR A PARTICULAR PURPOSE,
                  MERCHANTABILITY, MERCHANTABLE QUALITY, NON-INFRINGEMENT AND
                  ACCURACY AND NON-INTERFERENCE; (B) NEITHER REVELRY NOR ANY
                  REVELRY PARTY WARRANTS THAT (i) THE SITE IS OR WILL BE SECURE,
                  ACCURATE, COMPLETE, UNINTERRUPTED, WITHOUT ERROR, OR FREE OF
                  VIRUSES, WORMS, OTHER HARMFUL COMPONENTS, OR OTHER PROGRAM
                  LIMITATIONS, (ii) THE SITE WILL MEET YOUR REQUIREMENTS, (iii)
                  THE RESULTS THAT MAY BE OBTAINED FROM THE USE OF THE SITE WILL
                  BE ACCURATE OR RELIABLE, (iv) ANY ERRORS IN THE SITE WILL BE
                  CORRECTED; (C) YOU ASSUME THE ENTIRE COST OF ALL NECESSARY
                  SERVICING, REPAIR, OR CORRECTION OF PROBLEMS CAUSED BY VIRUSES
                  OR OTHER HARMFUL COMPONENTS, UNLESS SUCH ERRORS OR VIRUSES ARE
                  THE DIRECT RESULT OF REVELRYS’ GROSS NEGLIGENCE OR WILLFUL
                  MISCONDUCT; (D) REVELRY AND THE REVELRY PARTIES, JOINTLY AND
                  SEVERALLY, DISCLAIM AND MAKE NO WARRANTIES OR REPRESENTATIONS
                  AS TO THE ACCURACY, QUALITY, RELIABILITY, SUITABILITY,
                  COMPLETENESS, TRUTHFULNESS, USEFULNESS, OR EFFECTIVENESS OF
                  THE FORMS, DATA, REPORTS, RESULTS OR OTHER INFORMATION
                  OBTAINED, GENERATED OR OTHERWISE RECEIVED BY YOU FROM
                  ACCESSING AND/OR USING THE SITE OR OTHERWISE RELATING TO THIS
                  AGREEMENT, AND (E) USE OF THE SITE IS ENTIRELY AT YOUR OWN
                  RISK AND NEITHER REVELRY NOR ANY OF THE REVELRY PARTIES SHALL
                  HAVE ANY LIABILITY OR RESPONSIBILITY THEREFOR.
                </span>
              </p>
              <p>
                <strong>
                  <span style={{ textDecoration: "underline" }}>
                    <span>Limitation of Liability</span>
                  </span>
                </strong>
              </p>
              <p>
                <span>
                  NOTWITHSTANDING ANYTHING TO THE CONTRARY IN THIS AGREEMENT, IN
                  NO EVENT WHATSOEVER SHALL REVELRY BE LIABLE FOR ANY INDIRECT,
                  SPECIAL, INCIDENTAL, PUNITIVE OR CONSEQUENTIAL DAMAGES,
                  INCLUDING BUT NOT LIMITED TO LOSS OF PROFITS, LOST TIME OR
                  GOOD WILL, EVEN IF REVELRY HAS BEEN ADVISED OF THE POSSIBILITY
                  OF SUCH DAMAGES, WHETHER IN CONTRACT, TORT (INCLUDING
                  NEGLIGENCE), STRICT LIABILITY OR OTHERWISE.&nbsp; REVELRY
                  SHALL NOT BE LIABLE FOR ANY CLAIMS AGAINST YOU BY THIRD
                  PARTIES. YOU ACKNOWLEDGE THAT THESE LIMITATIONS OF LIABILITY
                  SHALL APPLY EVEN IF THE REMEDIES FAIL THEIR ESSENTIAL PURPOSE.
                </span>
              </p>
              <p>
                <span>
                  User hereby acknowledges that this paragraph shall apply to
                  all content, merchandise and services available through the
                  Site. Certain states and/or jurisdictions do not allow the
                  exclusion of implied warranties or limitation of liability for
                  incidental, consequential or certain other types of damages,
                  so the exclusions set forth above may not apply to you.
                </span>
              </p>
              <p>
                <strong>
                  <span style={{ textDecoration: "underline" }}>
                    <span>Representations and Warranties</span>
                  </span>
                </strong>
              </p>
              <p>
                <span>
                  By using the Site, you represent and warrant that (a) you
                  understand and agree that this Agreement is a legally binding
                  agreement and the equivalent of a signed, written contract;
                  (b) you will use the Site in a manner consistent with all
                  applicable laws and regulations and in accordance with the
                  terms and conditions of this Agreement; (c) you are authorized
                  to sign for and bind any entity for whom you are acting a
                  representative or other agent; (d) you will not impersonate
                  any person or entity, misrepresent any affiliation with
                  another person, entity or association, use false headers or
                  otherwise conceal your identity from Revelry for any purpose.
                </span>
              </p>
              <p>
                <strong>
                  <span style={{ textDecoration: "underline" }}>
                    Indemnification
                  </span>
                </strong>
              </p>
              <p>
                <span>
                  You agree to indemnify, defend and hold harmless Revelry and
                  the Revelry Parties from and against any and all claims and
                  expenses, including reasonable attorneys’ fee, arising out of
                  or related in any way to your use of the Site, violation of
                  this Agreement, violation of any law or regulation or
                  violation of any proprietary or privacy right.
                </span>
              </p>
              <p>
                <strong>
                  <span style={{ textDecoration: "underline" }}>
                    Account Access
                  </span>
                </strong>
              </p>
              <p>
                <span>
                  Where use of the Site is contingent on you and your users
                  accessing an "account" and/or inserting a
                  "user-identification" and/or "password,” you agree that you
                  will be solely responsible for the user-ids and passwords that
                  are provided to you (as such passwords may be changed from
                  time to time in accordance with features of the Site) to
                  log-in.&nbsp; You and your users shall keep any correspondence
                  you receive relating to or through the use of the Site
                  (including, but not limited to, your user-id, passwords, and
                  other registration or sign-in information) confidential and in
                  a safe place and not disclose it to any third party.&nbsp; You
                  will be responsible and liable for all communications and
                  actions that take place through the use of your user-ids,
                  including without limitation, any actions that occur without
                  your authorization. Accordingly, it is your responsibility to
                  take appropriate actions immediately if any password has been
                  stolen, leaked, compromised or otherwise used without proper
                  consent.
                </span>
              </p>
              <p>
                <strong>
                  <span style={{ textDecoration: "underline" }}>
                    <span>Use by Children</span>
                  </span>
                </strong>
              </p>
              <p>
                <span>
                  The Site is intended for use by individuals 13 years of age or
                  older. Users under the age of 13 should get the assistance of
                  a parent or guardian.
                </span>
              </p>
              <p className="FreeForm">
                <strong>
                  <span style={{ textDecoration: "underline" }}>General</span>
                </strong>
              </p>
              <p className="FreeForm">
                <span>
                  This Agreement shall be governed by Texas law and controlling
                  United States federal law, without regard to the choice or
                  conflicts of law provisions of any jurisdiction
                </span>
                <span>
                  {" "}
                  or the United Nations Convention on the International Sale of
                  Goods
                </span>
                <span>
                  , and any disputes, actions, claims or causes of action
                  arising out of or in connection with this Agreement or the
                  Site, with the exception of claims for injunctive relief,
                  shall be resolved in
                </span>
                <span>
                  arbitration administered by the American Arbitration
                  Association and located in Austin, Texas.&nbsp; You may not
                  under any circumstances commence or maintain against Revelry
                  any class action, class arbitration, or other representative
                  action or proceeding. In the event that this arbitration
                  agreement is for any reason held to be unenforceable, any
                  litigation against Revelry may be commenced only in the
                  federal or state courts located in Travis County, Texas. You
                  hereby irrevocably consent to the jurisdiction of those courts
                  for such purposes.
                </span>
                &nbsp;
              </p>
              <p className="FreeForm">
                <span>
                  Any cause of action you may have with respect to your use of
                  the Site must be commenced within one (1) year after the claim
                  or cause of action arises.
                </span>
                &nbsp;
              </p>
              <p className="FreeForm">
                <span>
                  If any provision of this Agreement is held by a court of
                  competent jurisdiction to be invalid or unenforceable, then
                  such provisions shall be construed, as nearly as possible, to
                  reflect the intentions of the invalid or unenforceable
                  provisions, with all other provisions remaining in full force
                  and effect.
                </span>
              </p>
              <p className="FreeForm">
                <span>
                  It may be necessary for Revelry to perform scheduled or
                  unscheduled repairs, maintenance, or upgrades and such
                  activities may temporarily degrade the quality of the Site or
                  result in a partial or complete outage of the Site.&nbsp;
                  Revelry provides no assurance that you will receive advance
                  notification of such activities or that the Site will be
                  uninterrupted or error-free.&nbsp; Any degradation or
                  interruption in the Site shall not give rise to a refund or
                  credit of any fees paid by you.
                </span>
              </p>
              <p className="FreeForm">
                <span>
                  No joint venture, partnership, employment, or agency
                  relationship exists between you and Revelry as a result of
                  this agreement or use of the Site. The failure of Revelry to
                  enforce any right or provision in this Agreement shall not
                  constitute a waiver of such right or provision unless
                  acknowledged and agreed to by Revelry in writing.
                </span>
              </p>
              <p className="FreeForm">
                <span>
                  Neither party shall be liable to the other party for any
                  failure to perform any of its obligations (except payment
                  obligations) under this Agreement during any period in which
                  such performance is delayed by circumstances beyond its
                  reasonable control including, but not limited to, fire, flood,
                  war, embargo, strike, riot or the intervention of any
                  governmental authority.
                </span>
              </p>
              <p className="FreeForm">
                <span>
                  If you have not entered into another agreement with Revelry
                  regarding the subject matter contained herein, then this
                  Agreement comprises the entire agreement between you and
                  Revelry and supersedes all prior or contemporaneous
                  negotiations, discussions or agreements, whether written or
                  oral, between the parties regarding such subject matter.
                  However, if you and Revelry have entered into another
                  agreement regarding the subject matter set forth herein that
                  is a written and signed agreement between you and Revelry,
                  then this Agreement should be read and interpreted in
                  conjunction with such agreement and, in the event of a
                  conflict between this Agreement and a written, signed
                  agreement between the parties, the written, signed agreement
                  shall govern and control.
                </span>
              </p>
            </article>
          </div>
        </section>
      </main>
    </div>
  );
};

export default TermsPage;
