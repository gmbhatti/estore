import React from "react";

const AboutPage = () => {
  return (
    <div>
      <div class="container cms-page full-pages">
        <ul class="breadcrumbs">
          <li class="breadcrumb is-active">
            <a href="https://www.natori.com/" class="breadcrumb-label">
              Home
            </a>
          </li>
        </ul>
        <h2 class="page-heading">Privacy Policy</h2>
        <main class="page full">
          <p>Natori</p>
          <p>Privacy &amp; Usage Policy</p>
          <p>This document was last updated on May 26, 2017</p>
          <p>
            <strong>Acknowledgement and Acceptance of Terms:</strong> The Natori
            Company is committed to protecting your privacy. This Privacy and
            Usage Statement sets forth our current privacy and usage practices
            with regard to the information we collect about you when you or your
            computer interact with our website or visit our store. The
            information is used to enhance your shopping experience and is used
            to communicate with you about our products, services and promotions.
            By accessing and using{" "}
            <a href="https://www.natori.com/">www.natori.com</a>, you
            acknowledge, agree and fully understand The Natori Company’s Privacy
            &amp; Usage Statement, detailed below, and freely consent to the
            information collection and use practices described in this
            statement. Further, you agree that such policy is subject to all
            applicable laws and your use of such is undertaken at your own risk.
          </p>
          <p>
            <strong>Information We Collect and How We Use It:</strong> The
            Natori Company collects certain personal information from and about
            its users to enhance a customer’s shopping experience and to provide
            better services. We collect this information in at least three ways,
            including: directly from our Web Server logs, from Cookies, and from
            Users.
          </p>
          <p>
            <strong>My Account</strong> – On natori.com you have the opportunity
            to create a personal shopping account. This account allows you to
            check out quicker and easier. When you create this account you are
            authorizing The Natori Company to store your name, email address and
            any other information you have included when setting up the account.
          </p>
          <p>
            <strong>Email List</strong> – On natori.com you can sign up to
            receive promotional emails from us. Signing up to this email list
            can include but is not limited to such information as name, email
            address, mailing address, telephone numbers, gender, age, birthday
            and shopping preferences.
          </p>
          <p>
            <strong>Customer Service</strong> – Natori.com provides customer
            service to our customers and our representatives may ask for
            information such as name, email address, mailing address, telephone
            numbers and, if you make a purchase, credit card information to
            better assist you. If credit card information is required over the
            phone, The Natori Company does not store this information.
          </p>
          <p>
            <strong>Surveys, Sweepstakes &amp; Promotions</strong> – There are
            times on the website when you may be asked to participate in a
            survey, sweepstake and/or promotion. If you choose to participate,
            certain information such as name, email address, mailing address,
            telephone numbers, gender, age, birthday and shopping preferences
            may be asked. This information is for the benefit of the survey,
            sweepstake and/or promotion and if you have given your permission,
            communication may be sent about future promotions.
          </p>
          <p>&nbsp;</p>
          <p>
            <strong>Use of Personal information:</strong> Personal information
            collected on our website can be used for various purposes including
            but not limited to:
          </p>
          <ul>
            <li>
              To send communication to customers about Natori products,
              services, surveys and/or promotions
            </li>
            <li>
              Website and store improvement to enhance your shopping experience
            </li>
            <li>To track customer traffic</li>
            <li>Requested transaction processing</li>
            <li>Prevent and detect fraud and abuse</li>
            <li>
              Enable service providers to perform certain activities on our
              behalf
            </li>
            <li>Comply with legal obligations</li>
          </ul>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
        </main>
      </div>
    </div>
  );
};

export default AboutPage;
