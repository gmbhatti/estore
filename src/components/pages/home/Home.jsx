import React, { Component } from "react";
import "../../common/assets/css/style.css";
import Carrousal from "../../common/Carrousal/Carrousal";
import BannerUTube from "../../common/BannerUTube/BannerUTube";
import { ProductListing } from "./../../products/ProductListing";

export default class Home extends Component {
  render() {
    return (
      <div className="main" style={{ marginTop: -70 + "px" }}>
        <Carrousal />
        <section className="module-small">
          <div className="container">
            <div className="row">
              <div className="col-sm-6 col-sm-offset-3">
                <h2 className="module-title font-alt">Latest in clothing</h2>
              </div>
            </div>
            <div className="row multi-columns-row">
              <div className="col-sm-6 col-md-3 col-lg-3">
                <div className="shop-item">
                  <div className="shop-item-image">
                    <img
                      src="/images/shop/product-7.jpg"
                      alt="Accessories Pack"
                    />
                    <div className="shop-item-detail">
                      <a href="#" className="btn btn-round btn-b">
                        <span className="icon-basket">Add To Cart</span>
                      </a>
                    </div>
                  </div>
                  <h4 className="shop-item-title font-alt">
                    <a href="#">Accessories Pack</a>
                  </h4>
                  £9.00
                </div>
              </div>
            </div>
            <div className="row mt-30">
              <div className="col-sm-12 align-center">
                <a className="btn btn-b btn-round" href="#">
                  See all products
                </a>
              </div>
            </div>
            <ProductListing />
            <BannerUTube />
          </div>
        </section>
      </div>
    );
  }
}
