import React, { Component } from "react";

class Register extends Component {
  state = {
    registerFormFirstName: "",
    registerFormLastName: "",
    registerFormEmail: "",
    registerFormPassword: "",
    registerPhonePassword: "",
    registerFormNewsletter: false
  };

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    console.log(this.state);
  };
  render() {
    return (
      <div className="container col-md-6 col-md-offset-3 mt-5 mb-5">
        <form
          onSubmit={this.handleSubmit}
          className="text-center border border-light p-5"
        >
          <p className="h4 mb-4">Sign up</p>

          <div className="form-row mb-4">
            <div className="col">
              <input
                onChange={this.handleChange}
                type="text"
                id="registerFormFirstName"
                className="form-control"
                placeholder="First name"
              />
            </div>
            <div className="col">
              <input
                onChange={this.handleChange}
                type="text"
                id="registerFormLastName"
                className="form-control"
                placeholder="Last name"
              />
            </div>
          </div>

          <input
            onChange={this.handleChange}
            type="email"
            id="registerFormEmail"
            className="form-control mb-4"
            placeholder="E-mail"
          />

          <input
            onChange={this.handleChange}
            type="password"
            id="registerFormPassword"
            className="form-control"
            placeholder="Password"
            aria-describedby="registerFormPasswordHelpBlock"
          />
          <small
            id="registerFormPasswordHelpBlock"
            className="form-text text-muted mb-4"
          >
            At least 8 characters and 1 digit
          </small>

          <input
            onChange={this.handleChange}
            type="text"
            id="registerPhonePassword"
            className="form-control"
            placeholder="Phone number"
            aria-describedby="registerFormPhoneHelpBlock"
          />
          <small
            id="registerFormPhoneHelpBlock"
            className="form-text text-muted mb-4"
          >
            Optional - for two step authentication
          </small>

          <div className="custom-control custom-checkbox">
            <input
              onChange={this.handleChange}
              type="checkbox"
              className="custom-control-input"
              id="registerFormNewsletter"
            />
            <label
              className="custom-control-label"
              htmlFor="registerFormNewsletter"
            >
              Subscribe to our newsletter
            </label>
          </div>

          <button className="btn btn-info my-4 btn-block">Sign in</button>

          <p>or sign up with:</p>

          <a type="button" className="light-blue-text mx-2">
            <i className="fab fa-facebook-f" />
          </a>
          <a type="button" className="light-blue-text mx-2">
            <i className="fab fa-twitter" />
          </a>
          <a type="button" className="light-blue-text mx-2">
            <i className="fab fa-linkedin-in" />
          </a>
          <a type="button" className="light-blue-text mx-2">
            <i className="fab fa-github" />
          </a>
          <hr />
          <p>
            By clicking
            <em>Sign up</em> you agree to our
            <a href="" target="_blank">
              terms of service
            </a>
          </p>
        </form>
      </div>
    );
  }
}

export default Register;
