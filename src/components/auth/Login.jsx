import React, { Component } from "react";
import { axios } from 'axios';

class Login extends Component {
  state = {
    loginFormEmail: "",
    loginFormPassword: "",
    loginFormRemember: false
  };

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    console.log(this.state);
    axios.post('http://api.alizeh/user/login')
  };
  render() {
    return (
      <div className="container col-md-6 col-md-offset-3">
        <form
          onSubmit={this.handleSubmit}
          className="text-center border border-light p-5"
        >
          <p className="h4 mb-4">Sign in</p>
          <input
            onChange={this.handleChange}
            type="email"
            id="loginFormEmail"
            className="form-control mb-4"
            placeholder="E-mail"
          />

          <input
            onChange={this.handleChange}
            type="password"
            id="loginFormPassword"
            className="form-control mb-4"
            placeholder="Password"
          />

          <div className="d-flex justify-content-around">
            <div>
              <div className="custom-control custom-checkbox">
                <input
                  onChange={this.handleChange}
                  type="checkbox"
                  className="custom-control-input"
                  id="loginFormRemember"
                />
                <label
                  className="custom-control-label"
                  htmlFor="loginFormRemember"
                >
                  Remember me
                </label>
              </div>
            </div>
            <div>
              <a href="">Forgot password?</a>
            </div>
          </div>
          <button className="btn btn-info btn-block my-4">Sign in</button>
          <p>
            Not a member?
            <a href="">Register</a>
          </p>
          <p>or sign in with:</p>
          <a type="button" className="light-blue-text mx-2">
            <i className="fab fa-facebook-f" />
          </a>
          <a type="button" className="light-blue-text mx-2">
            <i className="fab fa-twitter" />
          </a>
          <a type="button" className="light-blue-text mx-2">
            <i className="fab fa-linkedin-in" />
          </a>
          <a type="button" className="light-blue-text mx-2">
            <i className="fab fa-github" />
          </a>
        </form>
      </div>
    );
  }
}
export default Login;
