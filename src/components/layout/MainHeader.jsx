import React, { Component } from "react";
import {
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavItem,
  MDBNavLink,
  MDBNavbarToggler,
  MDBCollapse,
  MDBFormInline,
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem,
  MDBIcon,
  MDBBtn,
  MDBBadge
} from "mdbreact";
import "./mainNavbar.css";
import { Link } from "react-router-dom";
import logo from "../../logo.png";
import { ProductConsumer } from "../../context";

class MainHeader extends Component {
  state = {
    isOpen: false
  };

  componentDidMount() {
    window.onscroll = function() {
      if (
        document.body.scrollTop > 80 ||
        document.documentElement.scrollTop > 80
      ) {
        document.getElementById("top_navbar").classList.add("top-nav-collapse");
        document
          .getElementById("top_navbar")
          .classList.add("rgba-stylish-strong");
      } else {
        document
          .getElementById("top_navbar")
          .classList.remove("top-nav-collapse");
        document
          .getElementById("top_navbar")
          .classList.remove("rgba-stylish-strong");
      }
    };
  }

  toggleCollapse = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  render() {
    return (
      <MDBNavbar
        id="top_navbar"
        dark
        expand="md"
        className="fixed-top scrolling-navbar"
      >
        <MDBNavbarBrand>
          <strong className="white-text">
            <Link to="/" className="white-text">
              <img
                src={logo}
                alt=""
                className="navbar-brand"
                style={{ maxHeight: 40 + "px" }}
              />
              Alizeh Store
            </Link>
          </strong>
        </MDBNavbarBrand>
        <MDBNavbarToggler onClick={this.toggleCollapse} />
        <MDBCollapse id="navbarCollapse3" isOpen={this.state.isOpen} navbar>
          <MDBNavbarNav left>
            <MDBNavItem>
              <MDBNavLink to="/">Home</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink to="/about">About</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink to="/contact">Contact</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink to="/cart">Cart</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink to="/list">Prod List</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink to="/product">Product</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink to="/products">Products</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink to="/compare">Compare</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink to="/masonery">Masonery</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink to="/checkout">Checkout</MDBNavLink>
            </MDBNavItem>
          </MDBNavbarNav>
          <MDBNavbarNav right>
            <MDBNavItem>
              <MDBNavLink to="/cart">
                <ProductConsumer>
                  {value => {
                    const { cart } = value;
                    if (cart.length > 0) {
                      const items = cart.length;
                      const loaded = true;
                      return (
                        <MDBIcon size="2x" icon="cart-plus">
                          <MDBBadge
                            size="1x"
                            color={loaded ? "danger" : "primary"}
                            className="ml-2"
                          >
                            {items}
                          </MDBBadge>{" "}
                        </MDBIcon>
                      );
                    }
                  }}
                </ProductConsumer>
              </MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink className="waves-effect waves-light" to="#!">
                <MDBIcon fab icon="twitter" />
              </MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink className="waves-effect waves-light" to="#!">
                <MDBIcon fab icon="google-plus-g" />
              </MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBDropdown>
                <MDBDropdownToggle nav caret>
                  <MDBIcon icon="user" />
                </MDBDropdownToggle>
                <MDBDropdownMenu className="dropdown-default" right>
                  <MDBDropdownItem to="/register">Register</MDBDropdownItem>
                  <MDBDropdownItem>
                    <Link to="/login">Login</Link>
                  </MDBDropdownItem>
                  <MDBDropdownItem>
                    <Link to="/register">Register</Link>
                  </MDBDropdownItem>
                  <MDBDropdownItem href="#!">Register</MDBDropdownItem>
                  <MDBDropdownItem href="#!">
                    Something else here
                  </MDBDropdownItem>
                </MDBDropdownMenu>
              </MDBDropdown>
            </MDBNavItem>
          </MDBNavbarNav>
        </MDBCollapse>
      </MDBNavbar>
    );
  }
}

export default MainHeader;
