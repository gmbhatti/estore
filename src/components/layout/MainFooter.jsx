import React from "react";
import { Link } from "react-router-dom";

const MainFooter = () => {
  return (
    <footer className="page-footer font-small">
      <div className="mdb-color lighten-3 pt-1">
        <div className="container text-center text-md-left">
          <div className="row text-center text-md-left mt-3 pb-3">
            <div className="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
              <h6 className="text-uppercase mb-4 font-weight-bold">
                Company name
              </h6>
              <p>
                Here you can use rows and columns here to organize your footer
                content. Lorem ipsum dolor sit amet, consectetur adipisicing
                elit.
              </p>
            </div>

            <hr className="w-100 clearfix d-md-none" />

            <div className="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
              <h6 className="text-uppercase mb-4 font-weight-bold">Products</h6>
              <p>
                <a href="#!">MDBootstrap</a>
              </p>
              <p>
                <a href="#!">MDWordPress</a>
              </p>
              <p>
                <a href="#!">BrandFlow</a>
              </p>
              <p>
                <a href="#!">Bootstrap Angular</a>
              </p>
            </div>

            <hr className="w-100 clearfix d-md-none" />

            <div className="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
              <h6 className="text-uppercase mb-4 font-weight-bold">
                Useful links
              </h6>
              <p>
                <Link to="/terms">Terms & conditions</Link>
              </p>
              <p>
                <Link to="/privacy">Privacy policy</Link>
              </p>
              <p>
                <a href="#!">Shipping Rates</a>
              </p>
              <p>
                <a href="#!">Help</a>
              </p>
            </div>

            <hr className="w-100 clearfix d-md-none" />

            <div className="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
              <h6 className="text-uppercase mb-4 font-weight-bold">Contact</h6>
              <p>
                <i className="fas fa-home mr-3" /> Lahore - 39250, PK
              </p>
              <p>
                <i className="fas fa-envelope mr-3" /> info@alizeh.pk
              </p>
              <p>
                <i className="fas fa-phone mr-3" /> + 92 234 567 88
              </p>
              <p>
                <i className="fas fa-print mr-3" /> + 01 234 567 89
              </p>
            </div>
          </div>
        </div>
      </div>

      <div className="d-flex align-items-center mdb-color special-color-dark pt-2">
        <div className="container">
          <div className="row">
            <div className="col-md-7 col-lg-8">
              <p
                className="text-center text-md-left "
                style={{ color: "white" }}
              >
                © 2019 Copyright:
                <a href="http://konvenience.net/">
                  <strong> Konvenience.net</strong>
                </a>
              </p>
            </div>

            <div className="col-md-5 col-lg-4 ml-lg-0 ">
              <div className="text-center text-md-right">
                <ul className="list-unstyled list-inline text-center ">
                  <li className="list-inline-item">
                    <a className="btn-floating btn-fb mx-1">
                      <i className="fab fa-facebook-f"> </i>
                    </a>
                  </li>
                  <li className="list-inline-item">
                    <a className="btn-floating btn-tw mx-1">
                      <i className="fab fa-twitter"> </i>
                    </a>
                  </li>
                  <li className="list-inline-item">
                    <a className="btn-floating btn-gplus mx-1">
                      <i className="fab fa-google-plus-g"> </i>
                    </a>
                  </li>
                  <li className="list-inline-item">
                    <a className="btn-floating btn-li mx-1">
                      <i className="fab fa-linkedin-in"> </i>
                    </a>
                  </li>
                  <li className="list-inline-item">
                    <a className="btn-floating btn-dribbble mx-1">
                      <i className="fab fa-dribbble"> </i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default MainFooter;
