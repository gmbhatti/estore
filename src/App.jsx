import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import "./App.css";

import Home from "./components/pages/home/Home";
import Carrousal from "./components/common/Carrousal/Carrousal";
import NotFound from "./components/common/NotFound";
import ProductList from "./components/product/ProductList";
import ProductDetail from "./components/products/ProductDetail";
import Cart from "./components/cart/Cart";
import Model from "./components/product/Model";
import PSSlider from "./components/common/PhotoSwipe/PSSlider";
import LBSlider from "./components/common/Lightbox/LBSlider";
import MainHeader from "./components/layout/MainHeader";
import MainFooter from "./components/layout/MainFooter";
import ContactPage from "./components/pages/ContactPage";
import AboutPage from "./components/pages/AboutPage";
import TermsPage from "./components/pages/TermsPage";
import PrivacyPage from "./components/pages/PrivacyPage";
import { Checkout } from "./components/cart/Checkout";
import Register from "./components/auth/Register";
import Login from "./components/auth/Login";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <MainHeader />
        <main>
          <Switch>
            <Route path="/slider" component={Carrousal} />
            <Route path="/psslider" component={PSSlider} />
            <Route path="/lightbox" component={LBSlider} />
            <Route exact path="/" component={Home} />
            <Route exact path="/products" component={ProductList} />
            <Route path="/product" component={ProductDetail} />
            <Route path="/cart" component={Cart} />
            <Route path="/contact" component={ContactPage} />
            <Route path="/about" component={AboutPage} />
            <Route path="/terms" component={TermsPage} />
            <Route path="/privacy" component={PrivacyPage} />
            <Route path="/checkout" component={Checkout} />
            <Route path="/register" component={Register} />
            <Route path="/login" component={Login} />
            <Route component={NotFound} />
          </Switch>
        </main>
        <MainFooter />
        <Model />
      </React.Fragment>
    );
  }
}

export default App;
