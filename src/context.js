import React, {
  Component
} from "react";
import {
  storeProducts,
  detailProduct
} from "./data";
import {
  productList,
  productDetails,
  productsData
} from "./data.products";


const ProductContext = React.createContext();

//Provider
class ProductProvider extends Component {
  state = {
    products: [],
    detailProduct: detailProduct,
    cart: [], //old
    modelOpen: false,
    modelProduct: detailProduct,
    cartSubTotal: 0,
    cartTax: 0,
    cartTotal: 0
  };

  componentDidMount() {
    console.log('List', productList)
    console.log('data', productsData)
    // productList.data.map(p => (console.log(p)))
    this.setInitialCart();
    this.setProducts();
  }

  //set by me
  setInitialCart = () => {
    if (localStorage.getItem("alizehLocalCart") !== null) {
      let tempCartItems = JSON.parse(localStorage.getItem("alizehLocalCart"));
      this.setState(() => {
        return {
          cart: tempCartItems,
          cartSubTotal: localStorage["alizehLocalCartSubTotal"],
          cartTax: localStorage["alizehLocalCartTax"],
          cartTotal: localStorage["alizehLocalCartTotal"]
        };
      });
    }
  };

  setProducts = () => {
    let tempProducts = [];
    storeProducts.forEach(item => {
      item.inCart = this.itemInCart(item.id);
      const singleItem = {
        ...item
      };
      tempProducts = [...tempProducts, singleItem];
    });
    this.setState(() => {
      return {
        products: tempProducts
      };
    });
  };

  itemInCart = id => {
    if (localStorage.getItem("alizehLocalCart") !== null) {
      let tempCartItems = JSON.parse(localStorage.getItem("alizehLocalCart"));
      const product = tempCartItems.find(item => item.id === id);
      return product ? true : false;
    }
    return false;
  };

  getItem = id => {
    const product = this.state.products.find(item => item.id === id);
    return product;
  };
  handleDetail = id => {
    const product = this.getItem(id);
    this.setState(() => {
      return {
        detailProduct: product
      };
    });
  };
  addToCart = id => {
    let tempProducts = [...this.state.products];
    const index = tempProducts.indexOf(this.getItem(id));

    const product = tempProducts[index];
    product.inCart = true;
    product.count = 1;
    const price = product.price;
    product.total = price;

    this.setState(
      () => {
        return {
          products: tempProducts,
          cart: [...this.state.cart, product]
        };
      },
      () => {
        this.addTotals();
      }
    );
  };
  openModel = id => {
    const product = this.getItem(id);
    this.setState(() => {
      return {
        modelProduct: product,
        modelOpen: true
      };
    });
  };
  closeModel = () => {
    this.setState(() => {
      return {
        modelOpen: false
      };
    });
  };
  increment = id => {
    let tempCart = [...this.state.cart];
    const selectedProduct = tempCart.find(item => item.id === id);

    const index = tempCart.indexOf(selectedProduct);
    const product = tempCart[index];

    product.count = product.count + 1;
    product.total = product.count * product.price;

    this.setState(
      () => {
        return {
          cart: [...this.state.cart]
        };
      },
      () => {
        this.addTotals();
      }
    );
  };
  decrement = id => {
    let tempCart = [...this.state.cart];
    const selectedProduct = tempCart.find(item => item.id === id);

    const index = tempCart.indexOf(selectedProduct);
    const product = tempCart[index];

    product.count = product.count - 1;
    if (product.count === 0) {
      this.removeItem(id);
    } else {
      product.total = product.count * product.price;
      this.setState(
        () => {
          return {
            cart: [...this.state.cart]
          };
        },
        () => {
          this.addTotals();
        }
      );
    }
  };
  removeItem = id => {
    let tempProducts = [...this.state.products];
    let tempCart = [...this.state.cart];

    tempCart = tempCart.filter(item => item.id !== id);

    const index = tempProducts.indexOf(this.getItem(id));
    let removedProduct = tempProducts[index];

    removedProduct.inCart = false;
    removedProduct.count = 0;
    removedProduct.total = 0;

    this.setState(
      () => {
        return {
          cart: [...tempCart],
          products: [...tempProducts]
        };
      },
      () => {
        this.addTotals();
      }
    );
  };
  clearCart = () => {
    this.setState(
      () => {
        return {
          cart: []
        };
      },
      () => {
        this.setProducts();
        this.addTotals();
      }
    );
  };
  addTotals = () => {
    let subTotal = 0;
    this.state.cart.map(item => (subTotal += item.total));
    const tempTax = subTotal * 0.1;
    const tax = parseFloat(tempTax.toFixed(2));
    const total = subTotal + tax;
    this.setState(() => {
      return {
        cartSubTotal: subTotal,
        cartTax: tax,
        cartTotal: total
      };
    });
    this.updateAlizehLocalCart();
  };
  //added by me
  updateAlizehLocalCart = () => {
    localStorage["alizehLocalCart"] = JSON.stringify(this.state.cart);
    localStorage["alizehLocalCartSubTotal"] = this.state.cartSubTotal;
    localStorage["alizehLocalCartTax"] = this.state.cartTax;
    localStorage["alizehLocalCartTotal"] = this.state.cartTotal;
  };
  render() {
    return ( <
      ProductContext.Provider value = {
        {
          ...this.state,
          handleDetail: this.handleDetail,
          addToCart: this.addToCart,
          openModel: this.openModel,
          closeModel: this.closeModel,
          increment: this.increment,
          decrement: this.decrement,
          removeItem: this.removeItem,
          clearCart: this.clearCart
        }
      } > {
        " "
      } {
        this.props.children
      } {
        " "
      } <
      /ProductContext.Provider>
    );
  }
}
//Consumer
const ProductConsumer = ProductContext.Consumer;

export {
  ProductProvider,
  ProductConsumer
};